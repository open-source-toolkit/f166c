# 编译原理实验一：词法分析器设计

## 项目描述

本项目提供了一个词法分析器的源代码和实验报告，旨在帮助学习编译原理的学生理解和实践词法分析的基本概念和实现方法。词法分析器的主要功能是从输入的源程序中识别出各个具有独立意义的单词，包括基本保留字、标识符、常数、运算符和分隔符五大类，并依次输出各个单词的种别码及单词符号自身值。

## 资源内容

- **源代码**：包含词法分析器的完整实现代码，支持从txt文件中读取源程序并进行词法分析。
- **实验报告**：详细描述了实验的设计思路、DFA转换图、数据结构、函数调用关系流程图以及实验总结和思考题的回答。
- **流程图**：展示了词法分析器的工作流程和状态转换。
- **表格**：列出了词法分析器识别的单词类型及其对应的种别码。
- **测试文件**：提供了多个测试用例，用于验证词法分析器的正确性和鲁棒性。

## 功能实现

1. **输入**：从txt文件中读取源程序。
2. **输出**：识别并输出源程序中的各个单词，格式为（种别码，单词符号自身值）。每个单词单独占一行。
3. **错误处理**：遇到无法识别的单词时，输出“Error”并跳过错误部分继续分析。

## 实验过程

1. **DFA转换图**：设计了词法分析器的DFA转换图，用于指导程序的实现。
2. **数据结构**：采用了适当的数据结构来存储和处理识别出的单词，并重载了输出函数以便于展示结果。
3. **函数调用关系**：绘制了函数调用关系的流程图，清晰展示了程序的执行流程。

## 实验总结

在实验过程中，我们发现了一些可以优化的环节，例如在判断关键字和界符时，可以通过改进算法来提高效率。通过本次实验，我们深入理解了词法分析的基本原理和实现方法，并掌握了如何通过优化算法来提高程序的性能。

## 思考题回答

**程序设计中哪些环节影响词法分析的效率？如何提高效率？**

在词法分析器的设计中，关键字和界符的匹配是影响效率的重要环节。目前的方法是将字符串读取完后存放在字符数组中再逐个与关键字表进行匹配，这种方式效率较低。可以通过在读取字符的同时进行判断，从而减少不必要的字符串操作，提高匹配效率。此外，界符的匹配也可以采用类似的方法进行优化。

## 使用说明

1. 克隆或下载本仓库到本地。
2. 打开源代码文件，根据需要修改输入文件路径。
3. 编译并运行程序，查看词法分析结果。
4. 参考实验报告，深入理解词法分析的实现细节。

## 贡献

欢迎对本项目进行改进和扩展，如果您有任何建议或发现了问题，请提交Issue或Pull Request。

## 许可证

本项目采用MIT许可证，详情请参阅[LICENSE](LICENSE)文件。